<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trailer extends Model
{

    protected $table = 'Trailer';
    public $timestamps = true;

}
