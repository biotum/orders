<?php

namespace App\Models;

use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Driver extends Model
{
    use ModelTree;

    protected $table = 'Driver';
    public $timestamps = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('id');
        $this->setOrderColumn('id');
        $this->setTitleColumn('LastName');
    }

    public function drivervehicle() : BelongsTo
    {
        return $this->belongsTo(DriverVehicle::class,'id ');
    }
}
