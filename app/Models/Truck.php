<?php

namespace App\Models;

use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Truck extends Model
{

    protected $table = 'Truck';
    public $timestamps = true;
    use ModelTree;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('id_vehicle');
        $this->setOrderColumn('id');
        $this->setTitleColumn('Model');
    }

    public function drivervehicle() : BelongsTo
    {
        return $this->belongsTo(new DriverVehicle(),'id_vehicle');
    }


//    /**
//     * Log belongs to users.
//     *
//     * @return BelongsTo
//     */
//    public function vehicle() : BelongsTo
//    {
//        return $this->belongsTo(new Vehicle(),'Id_vehicle');
//    }



}
