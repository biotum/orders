<?php

namespace App\Models;


use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DriverVehicle extends Model
{
    use ModelTree;

    protected $table = 'Driver_vehicle';
    public $timestamps = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('id');
        $this->setOrderColumn('id');
        $this->setTitleColumn('	StartTime');
    }



    public function truck() : BelongsTo
    {
        return $this->belongsTo(new Truck(),'id_vehicle','id_vehicle');
    }

    /**
     * Log belongs to users.
     *
     * @return BelongsTo
     */
    public function driver() : BelongsTo
    {
        return $this->belongsTo(new Driver(),'Id_driver');
    }

    /**
     * Log belongs to users.
     *
     * @return BelongsTo
     */
    public function venicle() : BelongsTo
    {
        return $this->belongsTo(new Vehicle(),'id_vehicle');
    }
}
