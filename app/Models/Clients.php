<?php

namespace App\Models;


use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{

    use ModelTree;

    protected $table = 'admin_Clients';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

       $this->setParentColumn('id');
        $this->setOrderColumn('id');
        $this->setTitleColumn('LastName');
    }

}
