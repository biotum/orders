<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicesTransportDetails extends Model
{

    protected $table = 'ServicesTransportDetails';
    public $timestamps = true;

}
