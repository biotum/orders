<?php

namespace App\Models;

use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Products extends Model
{
    use ModelTree;

    protected $table = 'Products';
    public $timestamps = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('id');
        $this->setOrderColumn('id');
        $this->setTitleColumn('name');
    }

    public function productstorage() : BelongsTo
    {
        return $this->belongsTo(ProductStorage::class,'id_product');
    }
}
