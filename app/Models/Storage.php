<?php

namespace App\Models;

use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    use ModelTree;
    protected $table = 'storage';
    public $timestamps = true;
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('id');
        $this->setOrderColumn('id');
        $this->setTitleColumn('name');
    }
}
