<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicesDetails extends Model
{

    protected $table = 'ServicesDetails';
    public $timestamps = true;

}
