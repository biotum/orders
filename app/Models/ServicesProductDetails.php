<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicesProductDetails extends Model
{

    protected $table = 'ServicesProductDetails';
    public $timestamps = true;

}
