<?php

namespace App\Models;


use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Vehicle extends Model
{
    use ModelTree;

    protected $table = 'Vehicle';
    public $timestamps = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn('id');
        $this->setOrderColumn('id');
        $this->setTitleColumn('Producer');
    }

//    public function drivervehicle() : BelongsTo
//    {
//        return $this->belongsTo(DriverVehicle::class,'Id_vehicle');
//    }







}
