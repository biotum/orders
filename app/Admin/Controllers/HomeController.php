<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Clients;
use App\Models\Services;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Tree;


class HomeController extends AdminController
{
    use HasResourceActions;

    /**
     * {@inheritdoc}
     */
    protected function title()
    {
        return 'Рейсы';
    }
    public function grid()
    {
                $grid = new Grid(new Services());
                $grid->id('ID')->sortable();
                $grid->CreateDate();
                $grid->Deadline();
                $grid->actions(function (Grid\Displayers\Actions $actions) {
                    if ($actions->row->slug == 'administrator') {
                        $actions->disableDelete();
                    }
                });
                $grid->tools(function (Grid\Tools $tools) {
                    $tools->batch(function (Grid\Tools\BatchActions $actions) {
                        $actions->disableDelete();
                    });
                });

         return $grid;

    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {

            $form = new Form(new Services());
            $form->display('id');

            $form->text('FirstName');
            $form->text('LastName');

            $form->datetime('created_at');
            $form->datetime('updated_at');


        return $form;


    }

}
