<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('Driver_vehicle', function(Blueprint $table) {
			$table->foreign('Id_driver')->references('id')->on('Driver')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('Driver_vehicle', function(Blueprint $table) {
			$table->foreign('Id_vehicle')->references('Id_vehicle')->on('Vehicle')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('Truck', function(Blueprint $table) {
			$table->foreign('Id_vehicle')->references('Id_vehicle')->on('Vehicle')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('Trailer', function(Blueprint $table) {
			$table->foreign('Id_vehicle')->references('Id_vehicle')->on('Vehicle')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('Driver_vehicle', function(Blueprint $table) {
			$table->dropForeign('Driver_vehicle_Id_driver_foreign');
		});
		Schema::table('Driver_vehicle', function(Blueprint $table) {
			$table->dropForeign('Driver_vehicle_Id_vehicle_foreign');
		});
		Schema::table('Truck', function(Blueprint $table) {
			$table->dropForeign('Truck_Id_vehicle_foreign');
		});
		Schema::table('Trailer', function(Blueprint $table) {
			$table->dropForeign('Trailer_Id_vehicle_foreign');
		});
	}
}