<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehicleTable extends Migration {

	public function up()
	{
		Schema::create('Vehicle', function(Blueprint $table) {
            $table->increments('id',true);
			$table->timestamps();
            $table->integer('id_vehicle');
			$table->string('type_class', 8);
			$table->string('Producer', 16);
		});
	}

	public function down()
	{
		Schema::drop('Vehicle');
	}
}
