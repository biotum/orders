<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTruckTable extends Migration {

	public function up()
	{
		Schema::create('Truck', function(Blueprint $table) {
			$table->integer('id_vehicle')->unsigned();
			$table->timestamps();
			$table->string('RegNum', 12);
			$table->string('Model', 12);
			$table->string('AxelConfiguration', 8)->default('4x2,6x2');
		});
	}

	public function down()
	{
		Schema::drop('Truck');
	}
}
