<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductStorageTable extends Migration {

	public function up()
	{
		Schema::create('ProductStorage', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('id_storage');
			$table->integer('id_product');
			$table->decimal('quantity', 8,2);
		});
	}

	public function down()
	{
		Schema::drop('ProductStorage');
	}
}