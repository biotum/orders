<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration {

	public function up()
	{
		Schema::create('Services', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->datetime('CreateDate');
			$table->datetime('Deadline');
		});
	}

	public function down()
	{
		Schema::drop('Services');
	}
}