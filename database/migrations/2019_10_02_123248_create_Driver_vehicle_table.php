<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriverVehicleTable extends Migration {

	public function up()
	{
		Schema::create('Driver_vehicle', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('Id_driver')->unsigned();
			$table->integer('Id_vehicle')->unsigned();
			$table->datetime('StartTime');
			$table->datetime('FinishTime');
		});
	}

	public function down()
	{
		Schema::drop('Driver_vehicle');
	}
}