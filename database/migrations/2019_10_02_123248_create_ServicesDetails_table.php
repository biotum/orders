<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesDetailsTable extends Migration {

	public function up()
	{
		Schema::create('ServicesDetails', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('Id_services');
			$table->integer('Id_client_receiver');
			$table->integer('Id_client_sender');
		});
	}

	public function down()
	{
		Schema::drop('ServicesDetails');
	}
}