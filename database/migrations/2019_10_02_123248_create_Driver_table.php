<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDriverTable extends Migration {

	public function up()
	{
		Schema::create('Driver', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('LastName', 255);
			$table->string('FirstName', 255);
			$table->string('Contact', 255);
		});
	}

	public function down()
	{
		Schema::drop('Driver');
	}
}