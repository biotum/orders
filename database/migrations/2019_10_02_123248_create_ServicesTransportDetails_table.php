<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTransportDetailsTable extends Migration {

	public function up()
	{
		Schema::create('ServicesTransportDetails', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('Id_services');
			$table->integer('Id_driver');
			$table->datetime('SendTime');
			$table->datetime('DeliveryTime');
		});
	}

	public function down()
	{
		Schema::drop('ServicesTransportDetails');
	}
}