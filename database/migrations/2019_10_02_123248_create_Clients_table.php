<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration {

	public function up()
	{
		Schema::create('Clients', function(Blueprint $table) {
			$table->increments('id', true);
			$table->timestamps();
			$table->string('country', 255);
			$table->string('region', 255);
			$table->string('city', 255);
			$table->string('street', 255);
			$table->string('house', 10);
			$table->string('apartment', 10);
			$table->string('FirstName', 255);
			$table->string('postalcode', 10);
			$table->string('LastName', 255);
			$table->string('FirmName', 255);
			$table->string('Contact', 255);
		});
	}

	public function down()
	{
		Schema::drop('Clients');
	}
}