<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesProductDetailsTable extends Migration {

	public function up()
	{
		Schema::create('ServicesProductDetails', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('Id_services');
			$table->integer('id_product');
		});
	}

	public function down()
	{
		Schema::drop('ServicesProductDetails');
	}
}