<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrailerTable extends Migration {

	public function up()
	{
		Schema::create('Trailer', function(Blueprint $table) {
			$table->integer('id_vehicle')->unsigned();
			$table->timestamps();
			$table->string('RegNum', 8)->unique();
			$table->string('Model', 8);
			$table->string('TrailerType', 8);
		});
	}

	public function down()
	{
		Schema::drop('Trailer');
	}
}