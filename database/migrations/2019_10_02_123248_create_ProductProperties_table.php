<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductPropertiesTable extends Migration {

	public function up()
	{
		Schema::create('ProductProperties', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('type_quantity');
		});
	}

	public function down()
	{
		Schema::drop('ProductProperties');
	}
}